import sys
from PySide6.QtWidgets import QApplication
from clases.general import Access
from PySide6.QtCore import QFile, QTextStream


app = QApplication([])

style_app = QFile("css/app.css")
style_app.open(QFile.ReadOnly)
stream = QTextStream(style_app)
app.setStyleSheet(stream.readAll())

ventana_principal = Access()
ventana_principal.show()
sys.exit(app.exec())