from PySide6 import QtGui, QtCore
from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QFrame, QHBoxLayout, QMessageBox
from clases.register import Register
from clases.forget import Forget
from clases.home import Home
from clases.game import Game
from clases.services import Services

class Login(QWidget):

    def __init__(self, parent):
        super().__init__(parent)
        self.ventana_login = self
        self.setMinimumSize(150, 150)
        self.layout_login = QHBoxLayout(self)
        self.setLayout(self.layout_login)
        centro = parent.rect().center()
        self.move(centro - self.rect().center())

        self.layout_login_fields = None
        self.contenedor = QFrame(self)
        self.contenedor.setStyleSheet(
            "position: relative; z-index: 1; background: #FFFFFF; text-align: center; box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24); width: 250px;")

        self.layout_login_fields = QVBoxLayout(self.contenedor)

        self._createComponentLogin(parent)

    def _createComponentRegister(self):
        self._deleteComponentLogin()
        Register(self)
        Register._componentRegister(self)

    def _createComponentForget(self):
        self._deleteComponentLogin()
        Forget()
        Forget._componentForget(self)


    def _createComponentLogin(self, parent):
        self.label_usuario = QLabel("Usuario:")
        self.edit_usuario = QLineEdit()
        self.label_password = QLabel("Contraseña:")
        self.edit_password = QLineEdit()
        self.edit_password.setEchoMode(QLineEdit.Password)
        self.boton_ingreso = QPushButton("INGRESAR")
        self.boton_ingreso.clicked.connect(self._loginUser)
        self.register = QLabel('<a href="#">No estas registrado?</a>')
        self.register.linkActivated.connect(self.register_layout)
        self.forget = QLabel('<a href="#">Olvidaste tu contraseña?</a>')
        self.forget.linkActivated.connect(self.forget_layout)

        self.label_espera = QLabel("Cargando.....")
        self.label_espera.setWindowTitle('Cargando.....')
        self.label_espera.setGeometry(0, 0, parent.width(), parent.height())
        self.label_espera.setAlignment(Qt.AlignCenter)
        self.label_espera.setGeometry(600, 250, 300, 50)
        self.label_espera.hide()

        self._componentesLogin()

    def _createComponentHome(self, usuario=''):
        Home(self)
        Home._componentRanking(self, usuario)

    def _createComponentNewGame(self, point=0, correo ='', usuario=''):
        Game(self)
        Game._newGame(self, point, correo, usuario)



    def _deleteComponentRegister(self, band = 'CREATE'):
        Register(self)
        Register.deleteComponentRegister(self, band)

    def _deleteComponentForget(self):
        Forget(self)
        Forget.deleteComponentForget(self)

    def _deleteComponentLogin(self, band=''):

        for i in reversed(range(self.layout_login_fields.count())):
            widget = self.layout_login_fields.itemAt(i).widget()
            if widget is not None:
                self.layout_login_fields.removeWidget(widget)
                widget.deleteLater()

        if band == 'HOME':
            self._createComponentHome()

    def _componentesLogin(self):

        self.label_usuario.setStyleSheet("font-weight: bold; color: #333;")
        self.label_password.setStyleSheet("font-weight: bold; color: #333;")
        self.edit_usuario.setStyleSheet("font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.edit_password.setStyleSheet("font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.boton_ingreso.setStyleSheet("background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 4px;")
        self.register.setStyleSheet("color: #b3b3b3; font-size: 12px;")
        self.boton_ingreso.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))

        self.layout_login_fields.addWidget(self.label_usuario, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_usuario, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.label_password, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_password, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.boton_ingreso,  alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.register, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.forget, alignment=Qt.AlignHCenter)
        self.layout_login.addWidget(self.contenedor)

        self.layout_login.addLayout(self.layout_login_fields)
        self.layout_login.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

    def register_layout(self):
        self._createComponentRegister()

    def forget_layout(self):
        self._createComponentForget()

    def _registerUser(self):
        Register(self)
        Register.registerUser(self)

    def _recoveryPass(self):
        Forget(self)
        Forget.recoveryPass(self)

    def _loginUser(self):

        usuario = self.edit_usuario.text()
        password = self.edit_password.text()

        if not (usuario and password):
            QMessageBox.warning(self, 'Campos Obligatorios', 'Por favor, ingrese todos los campos.')
        else:
            data_response = Services._login(usuario, password)

            code_response = data_response['code']
            msg_response = data_response['message']

            if code_response == '200':
                self._deleteComponentLogin('HOME')
            else:
                QMessageBox.warning(self, 'Error', msg_response)