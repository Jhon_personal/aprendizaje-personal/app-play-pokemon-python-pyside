import requests
import random
from config.constantes import URL_SERVICES, URL_SERVICES_POKEMON

class Services():

    @staticmethod
    def _token(correo='', usuario=''):

        url = f"{URL_SERVICES}/token"
        data = {
            "CORREO": correo,
            "USUARIO": usuario,
        }

        response = requests.post(url, json=data)
        data_response = response.json()

        #print(data_response)

        return data_response

    @staticmethod
    def _register(correo, usuario, password):

        token = Services._token(correo, usuario)

        url = f"{URL_SERVICES}/register"
        data = {
            "CORREO": correo,
            "USUARIO": usuario,
            "PASSWORD": password
        }
        headers = {
            "Authorization": f"Bearer {token}"
        }

        response = requests.post(url, json=data, headers=headers)
        data_response = response.json()

        return data_response

    @staticmethod
    def _login(usuario, password):

        token = Services._token('', usuario)

        url = f"{URL_SERVICES}/login"
        data = {
            "USUARIO": usuario,
            "PASSWORD": password
        }
        headers = {
            "Authorization": f"Bearer {token}"
        }

        response = requests.post(url, json=data, headers=headers)
        data_response = response.json()

        return data_response

    @staticmethod
    def _ranking(usuario):

        token = Services._token('', usuario)
        url = f"{URL_SERVICES}/list"

        headers = {
            "Authorization": f"Bearer {token}"
        }

        response = requests.get(url, headers=headers)
        data_response = response.json()

        return data_response

    @staticmethod
    def _rankingUser(usuario):

        token = Services._token('', usuario)
        url = f"{URL_SERVICES}/pointsUser/{usuario}"

        headers = {
            "Authorization": f"Bearer {token}"
        }

        response = requests.get(url, headers=headers)
        data_response = response.json()

        return data_response

    @staticmethod
    def _recoveryPass(correo):

        token = Services._token(correo, '')
        url = f"{URL_SERVICES}/recovery"

        headers = {
            "Authorization": f"Bearer {token}"
        }

        data = {
            "CORREO": correo
        }

        response = requests.post(url, json=data, headers=headers)
        data_response = response.json()

        return data_response


    @staticmethod
    def _savePoints(correo, points, fecha):

        token = Services._token(correo, fecha)
        url = f"{URL_SERVICES}/points"

        headers = {
            "Authorization": f"Bearer {token}"
        }

        data = {
            "CORREO": correo,
            "PUNTAJE": points,
            "FECHA": fecha
        }

        response = requests.post(url, json=data, headers=headers)
        data_response = response.json()

        return data_response


    @staticmethod
    def _pokemonInfo():

        id_pokemon = random.randint(1, 1017)
        url = f"{URL_SERVICES_POKEMON}{id_pokemon}"

        response = requests.get(url)
        data_response = response.json()

        return data_response


    @staticmethod
    def _pokemonRandom():

        data_response = []

        for _ in range(3):

            id_pokemon = random.randint(1, 1017)
            url = f"{URL_SERVICES_POKEMON}{id_pokemon}"

            response = requests.get(url)
            response_pokemon = response.json()
            pokemon_info = {'id': response_pokemon['id'], 'name': response_pokemon['name']}
            data_response.append(pokemon_info)

        return data_response