from PySide6 import QtGui, QtCore
from PySide6.QtWidgets import QWidget, QLabel, QGridLayout, QPushButton
from clases.services import Services


class Home(QWidget):

    def _componentRanking(self, usuario):

        self.layout_home = QGridLayout()
        self.label_ranking = QLabel("Ranking Top 5")
        self.boton_nuevo_juego = QPushButton("NUEVO JUEGO")
        self.label_data_usuario = QLabel("Información del Usuario")
        self.label_data_puntaje = QLabel("Puntaje")
        self.label_data_posicion = QLabel("Posición Ranking")
        self.label_data_usuario_info = QLabel("Usuario")

        Home._componentesRanking(self, usuario)

    def _componentesRanking(self, newUser):

        if newUser == '':
            usuario = self.edit_usuario.text()
        else:
            usuario = newUser

        self.data_response = Services._ranking(usuario)
        data_response_ranking = Services._rankingUser(usuario)
        ranking = list(data_response_ranking['dataUser'].keys())[0]
        puntos = data_response_ranking['dataUser'][ranking]['puntos']
        correo_usuario = data_response_ranking['dataUser'][ranking]['correo']
        _usuario = data_response_ranking['dataUser'][ranking]['usuario']

        self.boton_nuevo_juego.clicked.connect(lambda: self._createComponentNewGame(0, correo_usuario, _usuario))

        self.label_data_puntaje.setText(f"Puntaje: {puntos}")
        self.label_data_posicion.setText(f"Posición Ranking: {ranking}")
        self.label_data_usuario_info.setText(f"Usuario: {_usuario}")
        #print(data_response_ranking)

        self.label_ranking.setStyleSheet("font-weight: bold; color: #333;")
        self.label_data_usuario.setStyleSheet("font-weight: bold; color: #333;")
        self.label_data_puntaje.setStyleSheet("font-weight: bold; color: #333;")
        self.label_data_posicion.setStyleSheet("font-weight: bold; color: #333;")
        self.label_data_usuario_info.setStyleSheet("font-weight: bold; color: #333;")
        self.boton_nuevo_juego.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.boton_nuevo_juego.setStyleSheet(
            "background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 4px;")

        self.data_user = self.data_response['dataUser']

        self.layout_home.addWidget(self.label_ranking, 0, 0, 1, 2)

        row = 1
        for key, value in self.data_user.items():
            self.table_ranking = QLabel(f"Puntos: {value['puntos']}, Usuario: {value['usuario']}")
            self.layout_home.addWidget(self.table_ranking, row, 0, 1, 2)
            row += 1

        self.layout_home.addWidget(self.boton_nuevo_juego, 0, 3, 2, 2)
        self.layout_home.addWidget(self.label_data_usuario, 0, 5, 2, 2)
        self.layout_home.addWidget(self.label_data_puntaje, 2, 5, 2, 2)
        self.layout_home.addWidget(self.label_data_posicion, 4, 5, 2, 2)
        self.layout_home.addWidget(self.label_data_usuario_info, 6, 5, 2, 2)
        self.layout_login_fields.addLayout(self.layout_home)


    def _deleteComponentHome(self):

        for i in reversed(range(self.layout_home.count())):
            widget = self.layout_home.itemAt(i).widget()
            if widget is not None:
                self.layout_home.removeWidget(widget)
                widget.deleteLater()
