from clases.services import Services
from PySide6 import QtGui, QtCore
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QMessageBox


class Forget(QWidget):

    def _componentForget(self):

        self.label_correo = QLabel("Correo:")
        self.edit_correo = QLineEdit()
        self.label_passNew = QLabel("Nueva contraseña:")
        self.edit_passNew = QLineEdit()
        self.edit_passNew.setReadOnly(True)
        self.label_passMsg = QLabel("Solo debe diligenciar el correo, si existe automaticamente se mostrara la nueva contraseña")
        self.boton_forget = QPushButton("RECUPERAR CUENTA")
        self.boton_forget.clicked.connect(self._recoveryPass)
        self.login = QLabel('<a href="#">Ya tienes cuenta?</a>')
        self.login.linkActivated.connect(self._deleteComponentForget)
        Forget._componentesForget(self)

    def _componentesForget(self):

        self.label_correo.setStyleSheet("font-weight: bold; color: #333;")
        self.label_passNew.setStyleSheet("font-weight: bold; color: #333;")
        self.label_passMsg.setStyleSheet("font-weight: bold; color: #333;")
        self.edit_correo.setStyleSheet(
            "font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.edit_passNew.setStyleSheet(
            "font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.boton_forget.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.boton_forget.setStyleSheet(
            "background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 4px;")
        self.login.setStyleSheet("color: #b3b3b3; font-size: 12px;")

        self.layout_login_fields.addWidget(self.label_correo, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_correo, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.label_passNew, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_passNew, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.label_passMsg, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.boton_forget, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.login, alignment=Qt.AlignHCenter)

    def deleteComponentForget(self):
        self.layout_login_fields.removeWidget(self.label_correo)
        self.label_correo.deleteLater()
        self.layout_login_fields.removeWidget(self.edit_correo)
        self.edit_correo.deleteLater()
        self.layout_login_fields.removeWidget(self.label_passNew)
        self.label_passNew.deleteLater()
        self.layout_login_fields.removeWidget(self.edit_passNew)
        self.edit_passNew.deleteLater()
        self.layout_login_fields.removeWidget(self.label_passMsg)
        self.label_passMsg.deleteLater()
        self.layout_login_fields.removeWidget(self.boton_forget)
        self.boton_forget.deleteLater()
        self.layout_login_fields.removeWidget(self.login)
        self.login.deleteLater()

        self._createComponentLogin()

    def recoveryPass(self):

        correo = self.edit_correo.text()

        if not (correo):
            QMessageBox.warning(self, 'Campos Obligatorios', 'Por favor, ingrese el correo.')
        else:
            data_response = Services._recoveryPass(correo)

            code_response = data_response['code']
            dataUser = data_response['dataUser']
            msg_response = data_response['message']

            if code_response == '200':
                self.edit_passNew.setText(f"{dataUser['pass']}")
            else:
                QMessageBox.warning(self, 'Error', msg_response)