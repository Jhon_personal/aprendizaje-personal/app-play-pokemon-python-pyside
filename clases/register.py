from clases.services import Services
from PySide6 import QtGui, QtCore
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QMessageBox


class Register(QWidget):

    def _componentRegister(self):

        self.label_usuario = QLabel("Usuario:")
        self.edit_usuario = QLineEdit()
        self.label_password = QLabel("Contraseña:")
        self.edit_password = QLineEdit()
        self.edit_password.setEchoMode(QLineEdit.Password)
        self.label_correo = QLabel("Correo:")
        self.edit_correo = QLineEdit()
        self.boton_registro = QPushButton("REGISTRAR")
        self.boton_registro.clicked.connect(self._registerUser)
        self.login = QLabel('<a href="#">Ya tienes cuenta?</a>')
        self.login.linkActivated.connect(lambda url: self._deleteComponentRegister())
        Register._componentesRegister(self)

    def _componentesRegister(self):

        self.label_usuario.setStyleSheet("font-weight: bold; color: #333;")
        self.label_password.setStyleSheet("font-weight: bold; color: #333;")
        self.edit_usuario.setStyleSheet(
            "font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.edit_password.setStyleSheet(
            "font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.label_correo.setStyleSheet("font-weight: bold; color: #333;")
        self.edit_correo.setStyleSheet("font-family: 'Roboto', sans-serif; outline: 0; background: #f2f2f2; width: 100%; border: 0; margin: 0 0 15px; padding: 15px; box-sizing: border-box; font-size: 14px; width: 200px")
        self.boton_registro.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.boton_registro.setStyleSheet("background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 4px;")
        self.login.setStyleSheet("color: #b3b3b3; font-size: 12px;")

        self.layout_login_fields.addWidget(self.label_usuario, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_usuario, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.label_password, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_password, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.label_correo, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.edit_correo, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.boton_registro, alignment=Qt.AlignHCenter)
        self.layout_login_fields.addWidget(self.login, alignment=Qt.AlignHCenter)


    def deleteComponentRegister(self, band):

        for i in reversed(range(self.layout_login_fields.count())):
            widget = self.layout_login_fields.itemAt(i).widget()
            if widget is not None:
                self.layout_login_fields.removeWidget(widget)
                widget.deleteLater()

        if band == 'CREATE':
            self._createComponentLogin()
        else:
            self._createComponentHome()

    def registerUser(self):

        usuario = self.edit_usuario.text()
        correo = self.edit_correo.text()
        password = self.edit_password.text()

        if not (usuario and correo and password):
            QMessageBox.warning(self, 'Campos Obligatorios', 'Por favor, ingrese todos los campos.')
        else:
            data_response = Services._register(correo, usuario, password)

            code_response = data_response['code']
            msg_response = data_response['message']

            if code_response == '200':
                self._deleteComponentRegister('HOME')
            else:
                QMessageBox.warning(self, 'Error', msg_response)

            #print(data_response)
