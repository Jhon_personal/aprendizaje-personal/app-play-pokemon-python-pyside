from PySide6.QtWidgets import QMainWindow
from clases.login import Login

class Access(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Play Pokemon')
        self.setGeometry(100, 100, 600, 600)
        self.showMaximized()

        self._ventanaLogin()

    def _ventanaLogin(self):
        ventana_login = Login(self)
        self.setCentralWidget(ventana_login)

