import random
from datetime import datetime

import requests
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QGridLayout, QWidget, QLabel, QPushButton
from clases.home import Home
from clases.services import Services

class Game(QWidget):

    def _newGame(self, point, correo, usuario):

        Home(self)
        Home._deleteComponentHome(self)
        Game._componentGame(self, point, correo, usuario)


    def _componentGame(self, point, correo, usuario):

        self.layout_home = QGridLayout()
        self.label_pokemon_title = QLabel("Quien es este pokemon ?")
        self.label_pokemon_result = QLabel("")
        self.label_puntaje = QLabel("Puntaje: 0")
        self.puntaje = point
        self.label_puntaje.setText(f"Puntaje: {self.puntaje}")
        self.boton_finalizar = QPushButton("FINALIZAR")
        self.boton_finalizar.clicked.connect(lambda: Game._finishGame(self, correo, usuario))
        self.boton_continuar = QPushButton("CONTINUAR")
        self.boton_continuar.clicked.connect(lambda: self._createComponentNewGame(self.puntaje, correo, usuario))
        self.boton_continuar.hide()
        self.empty_label = QLabel('')

        Game._componentesGame(self)


    def _componentesGame(self):

        img_pokemon = QLabel()

        self.label_espera.show()
        data_response = Services._pokemonInfo()

        img_poke = data_response['sprites']['other']['official-artwork']['front_default']
        id_poke = data_response['id']
        name_poke = data_response['name']
        pokemon_ok = {'id': id_poke, 'name': name_poke}

        self.data_response_random = Services._pokemonRandom()
        self.data_response_random.append(pokemon_ok)
        random.shuffle(self.data_response_random)

        response = requests.get(img_poke)
        img_data = response.content

        pixmap = QPixmap()
        pixmap.loadFromData(img_data)
        img_pokemon.setPixmap(pixmap)
        self.label_espera.hide()

        self.label_pokemon_result.setStyleSheet("font-weight: bold; color: #333; text-align: center; font-size: 32px")
        self.label_pokemon_title.setStyleSheet("font-weight: bold; color: #333; text-align: center; font-size: 26px")
        self.label_puntaje.setStyleSheet("font-weight: bold; color: #333; text-align: center; font-size: 18px")
        self.boton_finalizar.setStyleSheet(
            "font-weight: bold; font-family: 'Roboto', sans-serif; background: #333; color: #FFFFFF; font-size: 16px;")
        img_pokemon.setStyleSheet("width: 50 %;")
        self.boton_continuar.setStyleSheet(
            "font-weight: bold; font-family: 'Roboto', sans-serif; background: #333; color: #FFFFFF; font-size: 16px;")

        self.layout_home.addWidget(self.label_pokemon_title, 0, 0, 1, 4)
        self.layout_home.addWidget(img_pokemon, 1, 0, 1, 2)
        self.layout_home.addWidget(self.label_pokemon_result, 1, 2, 1, 2)

        row = 0
        for info in self.data_response_random:
            self.button_options = QPushButton(info['name'])
            self.button_options.clicked.connect(Game.clickedPokemon(self, info['id'], pokemon_ok))
            self.button_options.setStyleSheet(
                "background-color: #4CAF50; color: white; padding: 8px 16px; border: none; border-radius: 4px;")
            self.layout_home.addWidget(self.button_options, 2, row)
            row += 1

        self.layout_home.addWidget(self.empty_label, 3, 0)
        self.layout_home.addWidget(self.label_puntaje, 4, 0, 1, 2)
        self.layout_home.addWidget(self.boton_finalizar, 4, 3, 1, 1)
        self.layout_home.addWidget(self.boton_continuar, 4, 2, 1, 1)

        self.layout_login_fields.addLayout(self.layout_home)


    def clickedPokemon(self, idPokemon, pokemon_ok):
        def button_clicked():
            Game._selectPokemon(self, idPokemon, pokemon_ok)

        return button_clicked


    def _selectPokemon(self, idPokemon, pokemon_ok):

        if idPokemon == pokemon_ok['id']:
            self.label_pokemon_result.setText(f"Correcto, es {pokemon_ok['name']}")
            self.boton_continuar.show()
            self.puntaje = self.puntaje + 1
            self.label_puntaje.setText(f"Puntaje: {self.puntaje}")

            row = 0
            for info in self.data_response_random:
                widget = self.layout_home.itemAtPosition(2, row).widget()
                if widget is not None:
                    self.layout_home.removeWidget(widget)
                    widget.deleteLater()
                row += 1
        else:
            self.label_pokemon_result.setText(f"Error, era {pokemon_ok['name']}")
            self.boton_continuar.hide()

            row = 0
            for info in self.data_response_random:
                widget = self.layout_home.itemAtPosition(2, row).widget()
                if widget is not None:
                    self.layout_home.removeWidget(widget)
                    widget.deleteLater()
                row += 1


    def _finishGame(self, correo, usuario):

        fecha_actual = datetime.now()
        fecha = fecha_actual.strftime('%Y-%m-%d')

        Services._savePoints(correo, self.puntaje, fecha)

        for i in reversed(range(self.layout_home.count())):
            widget = self.layout_home.itemAt(i).widget()
            if widget is not None:
                self.layout_home.removeWidget(widget)
                widget.deleteLater()

        self._createComponentHome(usuario)
